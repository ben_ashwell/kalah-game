package uk.co.benashwell.kalah.model.builder;

import uk.co.benashwell.kalah.model.Board;
import uk.co.benashwell.kalah.model.Game;
import uk.co.benashwell.kalah.model.Player;

public class GameBuilder {
    private Player playerOne;
    private Player playerTwo;
    private Board board;

    public GameBuilder setPlayerOne(Player playerOne) {
        this.playerOne = playerOne;
        return this;
    }

    public GameBuilder setPlayerTwo(Player playerTwo) {
        this.playerTwo = playerTwo;
        return this;
    }

    public GameBuilder setBoard(Board board) {
        this.board = board;
        return this;
    }

    public Game build() {
        Game game = new Game();
        game.setBoard(board);
        game.setPlayerOne(playerOne);
        game.setPlayerTwo(playerTwo);
        return game;
    }
}
