package uk.co.benashwell.kalah.model;

import java.util.List;

public class Board {

    private List<Cup> cupList;

    public Board(List<Cup> cupList){
        this.cupList = cupList;
    }

    public List<Cup> getCupList() {
        return cupList;
    }

    public void setCupList(List<Cup> cupList) {
        this.cupList = cupList;
    }
}
