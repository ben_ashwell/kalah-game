package uk.co.benashwell.kalah.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Cup {

    private int stones;
    private Player ownedBy;

    public Cup(Player ownedBy, int stones) {
        this.ownedBy = ownedBy;
        this.stones = stones;
    }

    public void addStones(int stones) {
        this.stones += stones;
    }

    public int getStones() {
        return stones;
    }

    public void setStones(int stones) {
        this.stones = stones;
    }

    public Player getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(Player ownedBy) {
        this.ownedBy = ownedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Cup cup = (Cup) o;

        return new EqualsBuilder()
                .append(stones, cup.stones)
                .append(ownedBy, cup.ownedBy)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(stones)
                .append(ownedBy)
                .toHashCode();
    }
}
