package uk.co.benashwell.kalah.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;
import uk.co.benashwell.kalah.dto.MoveDto;
import uk.co.benashwell.kalah.service.GameService;

import javax.validation.Valid;

import static uk.co.benashwell.kalah.config.ResponseConstants.GAME_CREATED_RESPONSE;

/**
 * Controller to interact with a Kalah Game
 */
@Controller
public class GameController {

    @Autowired
    private GameService gameService;

    /**
     * Controller to start a game of Kalah
     */
    @PutMapping("/start")
    public ResponseEntity<String> startGame(){
        gameService.startGame();
        return new ResponseEntity(GAME_CREATED_RESPONSE, HttpStatus.CREATED);
    }

    /**
     * Controller method to make a move
     */
    @PutMapping("/move")
    public ResponseEntity<String> makeAMove(@Valid @ModelAttribute MoveDto moveDto) {
        return new ResponseEntity(gameService.move(moveDto.getCup()), HttpStatus.OK);
    }

}
