package uk.co.benashwell.kalah.service;

public interface GameService {

    /**
     * Start a game of Kalah
     */
    void startGame();

    String move(Integer cup);
}
