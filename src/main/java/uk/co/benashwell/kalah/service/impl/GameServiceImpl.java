package uk.co.benashwell.kalah.service.impl;

import org.springframework.stereotype.Service;
import uk.co.benashwell.kalah.exception.AlreadyRunningGameException;
import uk.co.benashwell.kalah.exception.GameHasNotStartedException;
import uk.co.benashwell.kalah.exception.InvalidCupException;
import uk.co.benashwell.kalah.model.*;
import uk.co.benashwell.kalah.model.builder.GameBuilder;
import uk.co.benashwell.kalah.service.GameService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static uk.co.benashwell.kalah.config.ResponseConstants.*;

@Service
public class GameServiceImpl implements GameService {

    private Game game = null;
    private Player activePlayer;
    private static final int CUPS = 12;
    private static final int STONES_PER_CUP = 4;
    private static final int STONES_PER_MANKALA = 0;

    /**
     * Start a game of Kalah
     * Currently if there is already a game in progress it will throw an exception
     */
    @Override
    public void startGame() {
        if (game == null) {
            game = setupGame();
        } else
            throw new AlreadyRunningGameException();
    }

    @Override
    public String move(Integer cup) {

        //ensure a game has started
        if (game == null)
            throw new GameHasNotStartedException();

        activePlayer = getTheActivePlayer();

        //ensure cup is valid
        if (cup == null || cup < 0 || cup > game.getBoard().getCupList().size() || !checkCupIsForActivePlayer(cup) || isCupAMankala(getCup(cup)))
            throw new InvalidCupException();

        //get the stones from the cup, then change the number in that cup to 0
        int stonesInCup = getCup(cup).getStones();
        if (stonesInCup == 0)
            throw new InvalidCupException();

        getCup(cup).setStones(0);

        //for each ball in the cup add one to the next cup
        int lastCup = moveStonesAcrossBoard(cup, stonesInCup);

        return handleBoardStateAfterMove(getCup(lastCup));
    }

    /**
     * Handle the board state after a move has been made
     *
     * @param lastCup the last cup that a stone was placed in
     * @return string information about the state of the board
     */
    private String handleBoardStateAfterMove(Cup lastCup) {
        String moveResult = SUCCESSFUL_MOVE;

        //no stones on one side of the board
        if (isGameOver()) {
            GameWinningInformation winningInformation = getWinningInformation();
            return String.format(GAME_HAS_FINISHED_FORMAT, winningInformation.getWinner(),
                    winningInformation.getPlayerOneScore(), winningInformation.getPlayerTwoScore());
        }

        //last cup was a mankala - making the assumption it is the players one
        if (isCupAMankala(lastCup))
            return LAST_CUP_WAS_A_MANKALA_NEW_TURN_RESPONSE;

        //landed on empty cup on active players side
        if (lastCup.getOwnedBy().equals(activePlayer) && lastCup.getStones() == 1) {
            //get opposite cup, add stones to last cup and set opposite cup to 0 stones
            int index = game.getBoard().getCupList().indexOf(lastCup);
            Cup oppositeCup = game.getBoard().getCupList().get(game.getBoard().getCupList().size() - (index + 1));
            lastCup.setStones(lastCup.getStones() + oppositeCup.getStones());
            oppositeCup.setStones(0);

            endPlayersTurn();
            return LANDED_IN_EMPTY_CUP_RESPONSE;
        }

        endPlayersTurn();
        return moveResult;
    }

    /**
     * end a players turn and set up to use next players turn
     */
    private void endPlayersTurn() {
        //todo currently maintaining two lists. is this necessary?
        activePlayer = game.getNextTurn();

        if (game.getNextTurn() == game.getPlayerOne())
            game.setNextTurn(game.getPlayerTwo());
        else
            game.setNextTurn(game.getPlayerOne());
    }

    /**
     * Calculate the Information for winning a game
     *
     * @return information for winning a game
     */
    private GameWinningInformation getWinningInformation() {
        GameWinningInformation gameWinningInformation = new GameWinningInformation();

        //move all stones to mankala
        List<Cup> mankalaList = getMankala(game.getBoard().getCupList());

        for (Cup cup : game.getBoard().getCupList()) {
            //check not a mankala, then go through and add stones to the relelvant mankala
            if (!(cup instanceof Mankala)) {
                for (Cup mankala : mankalaList) {
                    if (cup.getOwnedBy().equals(mankala.getOwnedBy())) {
                        mankala.addStones(cup.getStones());
                        cup.setStones(0);
                    }
                }
            }
        }

        //todo rethink the winning information, perhaps a list of scores or something plus no optional check, also name of player is object id - not readable etc
        gameWinningInformation.setPlayerOneScore(mankalaList.get(0).getStones());
        gameWinningInformation.setPlayerTwoScore(mankalaList.get(1).getStones());
        gameWinningInformation.setWinner(mankalaList.stream().max(Comparator.comparingInt(Cup::getStones)).get().getOwnedBy());

        return gameWinningInformation;

    }

    /**
     * get the Mankala out of a list of cups
     *
     * @param cupList list of cups
     * @return mankala from that list of cups
     */
    private List<Cup> getMankala(List<Cup> cupList) {
        return cupList.stream().filter(o -> o instanceof Mankala).collect(Collectors.toList());
    }

    /**
     * Check if the game is over
     *
     * @return boolean value for if the game is over
     */
    private boolean isGameOver() {
        boolean gameOver = true;
        for (Cup cup : game.getBoard().getCupList()) {

            //if we are at a Mankala then check if all before have been blank
            //if not, set game over back to true so it can be set to false on the next cup with stones
            if (cup instanceof Mankala) {
                if (gameOver)
                    return true;
                else
                    gameOver = true;
            } else if (cup.getStones() > 0)
                gameOver = false;
        }

        return false;
    }

    /**
     * Move a given amount of stones across the board from a starting cup onwards
     *
     * @param startingCup cup to move on from (will start from the next cup)
     * @param stonesInCup amount of stones to move
     * @return the last cup a stone was placed in
     */
    private int moveStonesAcrossBoard(int startingCup, int stonesInCup) {
        int currentCupNumber = startingCup;

        while (stonesInCup > 0) {

            //check if we are at the end of the game board
            if (currentCupNumber == game.getBoard().getCupList().size() - 1)
                currentCupNumber = 0;
            else
                currentCupNumber++;

            Cup currentCup = getCup(currentCupNumber);

            //check if it is a mankala, only add if you own, otherwise need to decrease i
            if (isCupAMankala(currentCup)) {
                Mankala mankala = (Mankala) currentCup;

                if (mankala.getOwnedBy() == activePlayer)
                    incrementCupsStones(currentCup);
                else
                    break;
            } else
                incrementCupsStones(currentCup);

            stonesInCup--;
        }
        return currentCupNumber;
    }

    /**
     * chceck if a cup is a Mankala or a cup
     *
     * @param cup cup number to go to
     * @return if it is a cup or not
     */
    private boolean isCupAMankala(Cup cup) {
        return cup instanceof Mankala;
    }

    /**
     * increment the amount of stones within a cup
     *
     * @param cup given cup to increment
     */
    private void incrementCupsStones(Cup cup) {
        int currentStones = cup.getStones();
        cup.setStones(currentStones + 1);
    }

    /**
     * Get a given cup
     *
     * @param cup cup number to get
     * @return Cup at that number
     */
    private Cup getCup(Integer cup) {
        try {
            return game.getBoard().getCupList().get(cup);
        } catch (NullPointerException e) {
            throw new InvalidCupException();
        }
    }

    /**
     * check that the cup provided is for the given active player
     *
     * @param cup cup value
     * @return is for the given player
     */
    private boolean checkCupIsForActivePlayer(Integer cup) {
        //if is 1 - 6 and player 1
        if (cup <= game.getBoard().getCupList().size() / 2 && activePlayer.equals(game.getPlayerOne()))
            return true;
        //if is 7 - 12 and player 2
        if (cup >= game.getBoard().getCupList().size() / 2 && activePlayer.equals(game.getPlayerTwo()))
            return true;
        else
            return false;
    }

    /**
     * get the active player for this turn
     *
     * @return active player
     */
    private Player getTheActivePlayer() {
        if (activePlayer == null && game.getNextTurn() == null) {
            game.setNextTurn(game.getPlayerTwo());
            activePlayer = game.getPlayerOne();

        }
        return activePlayer;
    }

    /**
     * set up the game board with the correct amount of cups, stones and players
     * @return the set up game
     */
    private Game setupGame(){
        //create each player and the Mankala for them
        Player playerOne = new Player();
        Mankala playerOneMankala = new Mankala(playerOne, STONES_PER_MANKALA);

        Player playerTwo = new Player();
        Mankala playerTwoMankala = new Mankala(playerTwo, STONES_PER_MANKALA);

        //build cups list
        List<Cup> cupsList = setupCupsList(playerOneMankala, playerTwoMankala);

        return new GameBuilder()
                .setPlayerOne(playerOne)
                .setPlayerTwo(playerTwo)
                .setBoard(new Board(cupsList))
                .build();
    }

    /**
     * set up a list of cups to hold a set amount of cups and mankala
     * @return list of cups
     */
    private List<Cup> setupCupsList(Mankala mankalaOne, Mankala mankalaTwo) {
        List<Cup> cups = new ArrayList<>();

        //create the first half for player ones side then add player one Mankala
        cups.addAll(setupCupsList(CUPS/2, mankalaOne.getOwnedBy()));
        cups.add(mankalaOne);

        //repeat for player 2
        cups.addAll(setupCupsList(CUPS/2, mankalaTwo.getOwnedBy()));
        cups.add(mankalaTwo);

        return cups;
    }

    /**
     * set up a given amount of cups
     * @param amount amount of cups
     * @return list of cups
     */
    private List<Cup> setupCupsList(int amount, Player player){
        List<Cup> cups = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            cups.add(new Cup(player, STONES_PER_CUP));
        }

        return cups;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }
}
