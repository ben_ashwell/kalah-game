package uk.co.benashwell.kalah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static uk.co.benashwell.kalah.exception.ErrorMessages.INVALID_CUP_PROVIDED_ERROR_MESSAGE;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = INVALID_CUP_PROVIDED_ERROR_MESSAGE)
public class InvalidCupException extends RuntimeException {
}
