package uk.co.benashwell.kalah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static uk.co.benashwell.kalah.exception.ErrorMessages.GAME_HAS_NOT_STARTED_YET_EXCEPTION;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = GAME_HAS_NOT_STARTED_YET_EXCEPTION)
public class GameHasNotStartedException extends RuntimeException {
}
