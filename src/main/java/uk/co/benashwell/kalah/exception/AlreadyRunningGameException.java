package uk.co.benashwell.kalah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static uk.co.benashwell.kalah.exception.ErrorMessages.GAME_ALREADY_IN_PROGRESS_ERROR_MESSAGE;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = GAME_ALREADY_IN_PROGRESS_ERROR_MESSAGE)
public class AlreadyRunningGameException extends RuntimeException {
}
