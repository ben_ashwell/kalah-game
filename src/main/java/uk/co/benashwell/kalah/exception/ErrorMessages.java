package uk.co.benashwell.kalah.exception;

/**
 * Contains all Error Messages to be used in exceptions etc
 */
public class ErrorMessages {

    private ErrorMessages() {
    }

    public static final String GAME_ALREADY_IN_PROGRESS_ERROR_MESSAGE = "Game is already in progress, please close or" +
            " finish that game first before creating a new one.";

    public static final String INVALID_CUP_PROVIDED_ERROR_MESSAGE = "Invalid cup provided.";

    public static final String GAME_HAS_NOT_STARTED_YET_EXCEPTION = "There is not a game in progress cup provided.";
}
