package uk.co.benashwell.kalah.config;

/**
 * Configuration to hold all Response Constants
 */
public class ResponseConstants {

    private ResponseConstants() {
    }

    public static final String GAME_CREATED_RESPONSE = "Game successfully created";
    public static final String SUCCESSFUL_MOVE = "Successful move";
    public static final String GAME_HAS_FINISHED_FORMAT = "Game Over - Player %s wins - %s - %s";
    public static final String LAST_CUP_WAS_A_MANKALA_NEW_TURN_RESPONSE = "The last cup a stone was placed in was a Mankala, player gets another turn";
    public static final String LANDED_IN_EMPTY_CUP_RESPONSE = "The last cup a stone was placed in was empty so you stole the stones opposite";
}
