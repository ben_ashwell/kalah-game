package uk.co.benashwell.kalah.dto;

public class MoveDto {

    private Integer cup;

    public Integer getCup() {
        return cup;
    }

    public void setCup(Integer cup) {
        this.cup = cup;
    }
}
