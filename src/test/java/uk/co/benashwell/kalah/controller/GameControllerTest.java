package uk.co.benashwell.kalah.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import uk.co.benashwell.kalah.exception.AlreadyRunningGameException;
import uk.co.benashwell.kalah.service.GameService;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.co.benashwell.kalah.config.ResponseConstants.GAME_CREATED_RESPONSE;

@ExtendWith(SpringExtension.class)
@WebMvcTest
public class GameControllerTest {

    public static final String START_ENDPOINT = "/start";
    @MockBean
    private GameService gameService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    //Start Game Tests

    @Test
    @DisplayName("Test that game service is called when starting a game")
    void startGameCallsService() throws Exception {
        mvc.perform(put(START_ENDPOINT));
        verify(gameService).startGame();
    }

    @Test
    @DisplayName("Test that expected status and content is returned")
    void startGameReturnsExpectedStaticString() throws Exception {
        mvc.perform(put(START_ENDPOINT))
                .andExpect(status().isCreated())
                .andExpect(content().string(GAME_CREATED_RESPONSE));
    }

    @Test
    @DisplayName("Ensure that when game already in progress the status")
    void startGameAlreadyRunningStatus() throws Exception {
        doThrow(AlreadyRunningGameException.class).when(gameService).startGame();
        mvc.perform(put(START_ENDPOINT))
                .andExpect(status().isBadRequest());
    }

    //Make a Move Tests

    @Test
    @DisplayName("Test that game service is called when making a move")
    void makeAMoveCallsService() throws Exception {
        mvc.perform(put("/move")
                .param("cup", "4"));

        verify(gameService).move(4);
    }

    @Test
    @DisplayName("Test that game service string return is in response")
    void makeAMoveReturnsResponseFromService() throws Exception {
        when(gameService.move(4)).thenReturn("test string");
        mvc.perform(put("/move")
                .param("cup", "4"))
                .andExpect(status().isOk())
                .andExpect(content().string("test string"));
    }

}