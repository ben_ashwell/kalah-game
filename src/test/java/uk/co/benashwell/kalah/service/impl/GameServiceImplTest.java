package uk.co.benashwell.kalah.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import uk.co.benashwell.kalah.exception.AlreadyRunningGameException;
import uk.co.benashwell.kalah.exception.InvalidCupException;
import uk.co.benashwell.kalah.model.*;
import uk.co.benashwell.kalah.model.builder.GameBuilder;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static uk.co.benashwell.kalah.config.ResponseConstants.*;

@ExtendWith(SpringExtension.class)
public class GameServiceImplTest {

    private GameServiceImpl gameService;
    private Player player1;
    private Player player2;

    @BeforeEach
    void setup(){
        gameService = new GameServiceImpl();

        gameService.setGame(createTestGame());
    }

    private Game createTestGame() {
        player1 = new Player();
        Mankala mankala1 = new Mankala(player1, 0);
        player2 = new Player();
        Mankala mankala2 = new Mankala(player2, 0);

        List<Cup> cups = Arrays.asList(
                new Cup(player1, 1),
                new Cup(player1, 4),
                mankala1,
                new Cup(player2, 1),
                new Cup(player2, 1),
                mankala2
        );

        return new GameBuilder()
                .setBoard(new Board(cups)).setPlayerOne(player1).setPlayerTwo(player2).build();
    }

    //Start Game tests

    @Test
    @DisplayName("start a game creates a new game")
    void startNewGame(){
        gameService.setGame(null);
        gameService.startGame();
        assertNotNull(gameService.getGame());
    }

    @Test
    @DisplayName("start a game when one already in place throws an exception")
    void startNewGameWhenOneAlreadyInPlace(){
        gameService.setGame(new Game());
        assertThrows(AlreadyRunningGameException.class, () -> gameService.startGame());
    }

    @Test
    @DisplayName("start a game sets up the game with players that are not null")
    void startNewGameSetsUpTheGamePlayers(){
        gameService.setGame(null);
        gameService.startGame();
        Game game = gameService.getGame();

        assertNotNull(game.getPlayerOne());
        assertNotNull(game.getPlayerTwo());

    }

    @Test
    @DisplayName("start a game sets up the game with a board containing cups and mankala")
    void startNewGameSetsUpTheGameBoard(){
        gameService.setGame(null);
        gameService.startGame();
        Game game = gameService.getGame();

        assertNotNull(game.getBoard());
        assertThat(game.getBoard().getCupList().size(), is(14));
        assertThat(game.getBoard().getCupList().get(0).getStones(), is(4));
        assertTrue(game.getBoard().getCupList().contains(new Mankala(game.getPlayerOne(), 0)));
        assertTrue(game.getBoard().getCupList().contains(new Mankala(game.getPlayerTwo(), 0)));
    }

    //move tests

    @Test
    @DisplayName("pass a null cup get an exception")
    void moveNullCup() {
        assertThrows(InvalidCupException.class,
                () -> gameService.move(null));
    }

    @Test
    @DisplayName("pass a negative cup value get an exception")
    void moveNegativeCup() {
        assertThrows(InvalidCupException.class,
                () -> gameService.move(-1));
    }

    @Test
    @DisplayName("pass a cup value over the size of the board get an exception")
    void moveHighCupValue() {
        assertThrows(InvalidCupException.class,
                () -> gameService.move(1000));
    }

    @Test
    @DisplayName("pass a cup value that is the other players")
    void moveOtherPlayersCup() {
        gameService.setActivePlayer(gameService.getGame().getPlayerTwo());
        assertThrows(InvalidCupException.class,
                () -> gameService.move(1));
    }

    @Test
    @DisplayName("pass a cup value that is a mankala")
    void moveStonesFromMankala() {
        assertThrows(InvalidCupException.class,
                () -> gameService.move(2));
    }

    @Test
    @DisplayName("move cup with 1 stone in it now has 0")
    void moveCupWith1StoneNowHas0() {
        gameService.move(0);
        assertThat(gameService.getGame().getBoard().getCupList().get(0).getStones(), is(0));
    }

    @Test
    @DisplayName("move cup with 1 stone in it, next cup now has one more")
    void moveCupWith1StoneNowHasOneMore() {
        gameService.move(0);
        assertThat(gameService.getGame().getBoard().getCupList().get(1).getStones(), is(5));
    }

    @Test
    @DisplayName("move cup with stones in it, adds stone to players mankala")
    void moveCupWithStonesAddsToPlayersMankala() {
        gameService.move(1);
        assertThat(gameService.getGame().getBoard().getCupList().get(2).getStones(), is(1));
    }

    @Test
    @DisplayName("move cup with stones in it, does not add to other players mankala")
    void moveCupWithStonesDoesNotAddToOtherPlayersMankala() {
        gameService.move(1);
        assertThat(gameService.getGame().getBoard().getCupList().get(5).getStones(), is(0));
    }

    @Test
    @DisplayName("move cup with stones in it, stone is moved and move complete")
    void moveCupWithStonesWhenLastStoneIsInANormalPoint() {
        assertThat(gameService.move(0), is(SUCCESSFUL_MOVE));
    }

    @Test
    @DisplayName("move cup with stones in it, last stone in mankala gives message")
    void moveCupWithStonesWhenLastStoneIsInMankalaGivesRelevantMessage() {
        gameService.setActivePlayer(player2);
        assertThat(gameService.move(4), is(LAST_CUP_WAS_A_MANKALA_NEW_TURN_RESPONSE));
    }

    @Test
    @DisplayName("move cup with stones in it, game over brings back game winning information ")
    void moveGameOverBringsBackWinningInformation() {
        gameService.setActivePlayer(player2);
        gameService.move(3);
        gameService.setActivePlayer(player2);
        assertThat(gameService.move(4), is(String.format(GAME_HAS_FINISHED_FORMAT, player1, 6, 1)));
    }

    @Test
    @DisplayName("move cup with stones in it, lands in empty cup")
    void moveLastStoneLandsInemptyCup() {
        gameService.setActivePlayer(player2);
        gameService.move(4);
        gameService.setActivePlayer(player2);
        assertThat(gameService.move(3), is(LANDED_IN_EMPTY_CUP_RESPONSE));
    }

    @Test
    @DisplayName("move cup with 0 stones in it throws an exception")
    void moveCupWithNoStonesIn() {
        gameService.setActivePlayer(player2);
        gameService.move(3);
        gameService.setActivePlayer(player2);
        assertThrows(InvalidCupException.class, () -> gameService.move(3));
    }
}