package uk.co.benashwell.kalah.model;


import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.*;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BoardTest {

    @Test
    @DisplayName("Test all getters and setters for POJO object")
    public void testGettersAndSetters() {
        ValidatorBuilder.create().with(new GetterMustExistRule()).with(new SetterMustExistRule())
                .with(new GetterTester()).with(new SetterTester()).with(new TestClassMustBeProperlyNamedRule())
                .with(new SerializableMustHaveSerialVersionUIDRule()).with(new NoStaticExceptFinalRule())
                .with(new NoPublicFieldsExceptStaticFinalRule()).build()
                .validate(PojoClassFactory.getPojoClass(Board.class));
    }
}