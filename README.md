# KALAH Game Project 
###### Ben Ashwell - baashwell@gmail.com

This project is a spring boot back end service that allows users to play a game of Kalah via REST endpoints. There is 
still much to be improved upon on this but at its basic a game can be started, played and won by a player.


## To Run

1. Clone the repository from bitbucket
2. Go in to the directory using terminal or command line
3. ```mvn clean package```
4. ```java -jar ./target/Kalah-0.0.1-SNAPSHOT.jar```

## To Play
### To start a game
Once the service is running use the /start endpoint to start a game of Kalah. The following is an example of a request 
made to a local running instance.
```
PUT localhost:8080/start
```

### To make a move
Once the service is running use the /move endpoint to make a move, along with the cup parameter to express the cup you 
would like to take stones out of and move. The following is an example of a request made to a local running instance, it 
will return a String that explains the outcome of the move.

Currently the cups are set out to go from 0 to 13 as the it moves around the board with Mankala at 6 and 13.

```
PUT localhost:8080/move?cup=5
```

## To do:
- Swagger docs
- Exception handler for error messages
- config class for values of cups etc
- Get Cups Endpoint to show current state of board
- A better way of handling the cup numbering

